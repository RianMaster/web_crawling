from requests import get
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from time import sleep
import os
from tkinter import Tk
from functools import partial
from tkinter import *
from tkinter.simpledialog import askstring
from shutil import copyfileobj
from base64 import b64decode
from selenium.common.exceptions import NoSuchElementException
from tkinter import messagebox


def image_download(keyword):
    global ext
    r = radioValue.get()
    if r == 1:
        ext = 'jpg'
    if r == 2:
        ext = 'jpeg'
    if r == 3:
        ext = 'png'

    global list
    list = keyword.get()

    keyword_list = list.split(',')

    PATH = './chromedriver'
    # 크롬을 띄우지 않는 옵션 설정
    options = webdriver.ChromeOptions()
    options.headless = False
    try:
        driver = webdriver.Chrome('C:/Chromedriver/chromedriver.exe', options=options)
        # 키워드 별로 반복문을 돌림

        for keyword in keyword_list:
            # 구글의 키워드 검색결과의 이미지 탭으로 가는 링크
            url = '''https://www.google.com/search?tbm=isch&source=hp&biw=&bih=&ei=0VgdX4viLcq9hwOB7IngCQ&q=''' + keyword.strip()

            driver.get(url)
            # 폴더가 없으면 키워드로 폴더를 만듦
            try:
                parent_dir = "./images/"
                path = os.path.join(parent_dir, keyword)
                os.mkdir(path)
                print('Path made for ' + keyword)

            except:
                print('Directory already exists for keyword:', keyword)
            # 페이지가 로딩이 느릴 때를 대비해 5초를 기다림
            sleep(5)
            # 다음 페이지의 가장 밑까지 가게한다
            last_height = driver.execute_script('return document.body.scrollHeight')
            while True:
                driver.execute_script(
                    'window.scrollTo(0,document.body.scrollHeight)')
                sleep(1)
                new_height = driver.execute_script(
                    'return document.body.scrollHeight')
                if new_height == last_height:
                    # 이미지를 더 로딩하는 버튼을 누름
                    try:
                        driver.find_element_by_class_name('mye4qd').click()
                        sleep(1)
                    except:
                        break
                last_height = new_height
            print('Done scrolling')

            # 페이지 내 모든 이미지를 찾음

            image_urls = driver.find_elements_by_css_selector('img.rg_i.Q4LuWd')
            print('Found', len(image_urls), 'results')
            messagebox.showinfo("이미지 갯수", "찾은 이미지 갯수는 " + str(len(image_urls)) + "입니다.")
            count = 0

            for image_thumbnail in image_urls:
                # 원본 이미지를 다운로드하기 위해 썸네일을 클릭함
                ActionChains(driver).click(image_thumbnail).perform()
                sleep(2)
                # 같은 클래스를 가진 이미지가 여러개이기 때문에 xpath를 통해 지정함
                try :
                    image_url = driver.find_element_by_xpath(
                        '/html/body/div[2]/c-wiz/div[3]/div[2]/div[3]/div/div/div[3]/div[2]/c-wiz/div/div[1]/div[1]/div/div[2]/a/img').get_attribute("src")
                    try:
                        with open(f'./images/{keyword}/{count}.{ext}', 'wb') as image_file:
                            # 이미지가 base64로 인코딩된 텍스트일때
                            #pass
                            #requests.get 함수를 사용해 링크에서 이미지(와 기타 등등)을 가져온다-stream은 안정성을 위해 추가
                            response = get(image_url, stream=True)
                            #이미지 데이터를 열고있는 파일에 저장한다.
                            copyfileobj(response.raw, image_file)
                            # 버퍼를 없앤다
                            del response
                            count += 1
                            print('Download Complete : ', count)
                        driver.implicitly_wait(2)

                    except :
                        print('Error')
                except NoSuchElementException:
                    print('NoSuchElementException')
            print('Done scrapping images')
    finally:
        # 브라우저를 닫고 더 이상의 셀레니움을 사용한 코드 실행을 멈춤
        driver.quit()


#확장자 선택 창
root = Tk()
root.title("구글 이미지 다운로더")
root.geometry("300x200")

lb00 = Label(root, text=" ").grid(row=0, sticky = W)
lb1 = Label(root, text=""
                       "검색을 원하는 키워드를 입력하세요.").grid(row=2, sticky = W)

keyword=StringVar()
word = entry_num1 = Entry(root, textvariable=keyword).grid(row=3, column=0)
lb0 = Label(root, text=" ").grid(row=4, sticky = W)

lb2 = Label(root, text= "원하는 확장자를 선택하세요.").grid(row=5, sticky = W)


radioValue = IntVar(value=1)
radiobtn1 = Radiobutton(root, text = 'jpg', variable=radioValue,value=1).grid(row=6, sticky = W)
radiobtn2 = Radiobutton(root, text = 'jpeg',variable=radioValue,value=2).grid(row=7, sticky = W)
radiobtn3 = Radiobutton(root, text = 'png', variable=radioValue,value=3).grid(row=8, sticky = W)

result = partial(image_download)
btn = Button(root, text="검색",command=lambda: image_download(keyword)).grid(row=3, column=1)

#ok_btn = Button(root, text="확인",command=click).grid(row=7, column=0)
#poke
root.mainloop()

